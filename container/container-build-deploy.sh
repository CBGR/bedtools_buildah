#!/bin/sh

# SPDX-FileCopyrightText: 2020-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# Copy the deployment scripts to a location that will be included in a package
# for this software. This needs to be done where the paths in a package are not
# used exclusively by a package, meaning that other packages might generate
# conflicting files and directories.

set -e

PROGNAME=$(basename "$0")
THISDIR=$(dirname "$0")
BASEDIR=$(realpath "$THISDIR/..")
DIRNAME=$(basename "$BASEDIR")
PACKAGE='bedtools'

# Container details.

CONTAINER=$1

# Require a container.

if [ ! "$CONTAINER" ] || [ "$CONTAINER" = '--help' ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME <container>

Make the deployment scripts available in the target location within the
container.
EOF
    exit 1
fi

# Access the container.

MNT=$(buildah mount "$CONTAINER")

# Define paths relative to the container.

APP="$MNT/var/www/apps/$DIRNAME"
APP_TARGET="$APP/bedtools/var/tmp/$PACKAGE"

if [ ! -e "$APP_TARGET" ] ; then
    mkdir -p "$APP_TARGET"
fi

# Copy the deployment scripts to the target location.

cp "$APP/deploy.sh" "$APP_TARGET"
cp -R "$APP/deploy" "$APP_TARGET"
cp -R "$APP/requirements" "$APP_TARGET"

buildah umount "$CONTAINER"

# vim: tabstop=4 expandtab shiftwidth=4
