#!/bin/sh

# SPDX-FileCopyrightText: 2020-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

set -e

PROGNAME=$(basename "$0")
THISDIR=$(dirname "$0")
BASEDIR=$(realpath "$THISDIR/..")
DIRNAME=$(basename "$BASEDIR")
WORKDIR="$BASEDIR/deploy/work"

# Container details.

CONTAINER=$1

# Require a container.

if [ ! "$CONTAINER" ] || [ "$CONTAINER" = '--help' ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME <container>

Build the bedtools software inside the indicated container. Typically, this
script is invoked by the more general build script.
EOF
    exit 1
fi

# Find any existing bedtools repository or obtain a repository.

REPO=$("$BASEDIR/deploy/tools/find_bedtools" "$WORKDIR")

if [ ! "$REPO" ] ; then
    exit 1
fi

DISTDIR=$(basename "$REPO")

# Access the container.

MNT=$(buildah mount "$CONTAINER")

# Define paths relative to the container.

APP="$MNT/var/www/apps/$DIRNAME"
APP_WORKDIR="$APP/deploy/work"

if [ ! -e "$APP_WORKDIR" ] ; then
    mkdir "$APP_WORKDIR"
fi

# Unpack the repository into the container.

if [ ! -e "$APP_WORKDIR/$DISTDIR" ] ; then
    "$BASEDIR/deploy/tools/unpack-repo.sh" "$REPO" "$APP_WORKDIR/$DISTDIR"
fi

buildah umount "$CONTAINER"

# Build and install bedtools inside the container.

APP="/var/www/apps/$DIRNAME"
WORKDIR="$APP/deploy/work"

buildah run "$CONTAINER" "$APP/deploy/tools/build_bedtools" "$WORKDIR/$DISTDIR" "$APP/bedtools"

# vim: tabstop=4 expandtab shiftwidth=4
