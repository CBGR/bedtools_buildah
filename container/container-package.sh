#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# This script extracts the software built and installed in a container to an
# archive outside the container.

set -e

PROGNAME=$(basename "$0")
THISDIR=$(realpath $(dirname "$0"))
BASEDIR=$(realpath "$THISDIR/..")
DIRNAME=$(basename "$BASEDIR")
PACKAGE='bedtools'

# Program parameters.

CONTAINER=${1:-"$PACKAGE"}
ARCHIVE=${2:-"$BASEDIR/$PACKAGE.tar.gz"}

if [ ! "$CONTAINER" ] || [ "$CONTAINER" = '--help' ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME <container> [ <archive> ]

Extract installed software from the given container, creating an archive of the
given name, or using a name derived from the package name.
EOF
    exit 1
fi

# Copy installed software from the container.

APP="/var/www/apps/$DIRNAME"

buildah unshare \
    "$BASEDIR/deploy/buildah/buildah-extract.sh" "$CONTAINER" "$APP" "$ARCHIVE" 'bedtools'

# vim: tabstop=4 expandtab shiftwidth=4
