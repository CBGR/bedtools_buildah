#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

# This script attempts to populate a container image with the bedtools software.
# However, it does not create a new Buildah image or produce an image for use
# with Docker.

set -e

THISDIR=$(realpath $(dirname "$0"))
BASEDIR=$(realpath "$THISDIR/..")
DIRNAME=$(basename "$BASEDIR")

# Program parameters.

CONTAINER=${1:-"bedtools"}

# Conveniences based on the parameters.

COPY="buildah copy $CONTAINER"
RUN="buildah run $CONTAINER"

# Although the which package is recorded as a requirement, the test of the
# system type uses the which command in order to determine which package
# management tools will be used, so the package is installed here as a
# bootstrapping measure.

$RUN dnf install -y which

# Clone this repository into the container.

APP="/var/www/apps/$DIRNAME"

buildah unshare \
    "$BASEDIR/deploy/buildah/buildah-unpack-repo.sh" "$CONTAINER" "$BASEDIR" "$APP"

# Install specific packages.

$RUN "$APP/deploy.sh" -p buildah

# Install the bedtools software.

buildah unshare \
    "$THISDIR/container-build-bedtools.sh" "$CONTAINER"

# Install the deployment scripts for post-installation tasks.

buildah unshare \
    "$THISDIR/container-build-deploy.sh" "$CONTAINER"

# vim: tabstop=4 expandtab shiftwidth=4
