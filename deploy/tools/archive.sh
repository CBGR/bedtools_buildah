#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

SOURCE=$(realpath "$1")
TARGET=$(realpath "$2")

shift 2

# Enter the installed software location and archive the files.

tar zcf "$TARGET" -C "$SOURCE" $*

# vim: tabstop=4 expandtab shiftwidth=4
