#!/bin/sh

# SPDX-FileCopyrightText: 2021-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

PROGNAME=$(basename "$0")
THISDIR=$(dirname "$0")
BASEDIR=$(realpath "$THISDIR/../..")

CONTAINER=$1
SOURCE=$2
TARGET=$3
CHANGESET=${4:-"HEAD"}

if [ ! "$CONTAINER" ] || [ "$CONTAINER" = '--help' ] || [ ! -e "$SOURCE" ] || \
   [ ! "$TARGET" ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME <container> <source repository> <target repository> [ <changeset> ]

Within the specified container, unpack the source repository in the target
location, updated to the indicated changeset/version (or to the HEAD changeset
if not specified).
EOF
    exit 1
fi

MNT=$(buildah mount "$CONTAINER")

"$BASEDIR/deploy/tools/unpack-repo.sh" "$SOURCE" "$MNT/$TARGET" "$CHANGESET"

buildah umount "$CONTAINER"

# vim: tabstop=4 expandtab shiftwidth=4
